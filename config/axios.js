import axios from "axios";

const axiosConect = axios.create({
  baseURL: process.env.NEXT_PUBLIC_URL_MARVEL,
});

axiosConect.interceptors.request.use((request) => {
  request.params = {
    ...request.params,
    ts: 1,
    apikey: process.env.NEXT_PUBLIC_MARVEL_KEY,
    hash: process.env.NEXT_PUBLIC_HASH_MARVEL,
  };
  return request;
});

export default axiosConect;
