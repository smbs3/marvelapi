import Axios from "axios";
import { makeUseAxios } from "axios-hooks";

const favorite = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_FAVORITE_API_URL,
});
const useFavorite = makeUseAxios({
  axios: favorite,
});
export default useFavorite;
