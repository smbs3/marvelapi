import Banner from "../banner";
import Card from "../card";
import CustomScroll from "../customScroll";
import Search from "../search";
import { useEffect, useState } from "react";
import { useDebounce } from "../../hook/useDebounce";
import axiosConect from "../../config/axios";
import Button from "../button";
import useFavorite from "../../config/axiosFavorite";
import { useSelector } from "react-redux";

const Characters = ({ results, getMore, hasMore }) => {
  const [search, setSearch] = useState("");
  const [searchActive, setSearchActive] = useState("name");
  const [filterList, setFilterList] = useState([]);
  const [favoriteList, setFavoriteList] = useState([]);
  const isLogged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [, refetch] = useFavorite(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const getFavoritesChar = async () => {
    if (!isLogged) return;
    const { data } = await refetch({
      params: {
        category: "CHARACTER",
      },
    });
    setFavoriteList(data);
  };

  useEffect(() => {
    getFavoritesChar();
  }, []);

  const searcher = (e) => {
    e.preventDefault();
    setSearch(e.target.value);
  };

  const getIsActive = (label) => {
    if (searchActive === label) return "isActive";
    return "";
  };

  useDebounce(
    () => {
      const searchCharacter = async () => {
        const value = search.trim();
        let params = { nameStartsWith: value, limit: 100 };
        if (value) {
          if (searchActive === "comic") {
            const { data } = await axiosConect.get("/comics", {
              params: { titleStartsWith: value },
            });
            const listId = data.data.results?.map((item) => item.id);
            params = { comics: listId.splice(0, 10).join(",") };
          }
          try {
            const { data } = await axiosConect.get(`/characters`, {
              params,
            });
            setFilterList(data.data.results);
          } catch (error) {}
        } else {
          setFilterList([]);
        }
      };
      searchCharacter();
    },
    800,
    [search]
  );
  const renderCharacters = () => {
    if (search) {
      return filterList?.map((result) => (
        <Card
          data={result}
          type="characters"
          category="CHARACTER"
          key={result.id}
          favoriteList={favoriteList}
        />
      ));
    } else {
      return results?.map((result) => (
        <Card
          data={result}
          type="characters"
          category="CHARACTER"
          key={result.id}
          favoriteList={favoriteList}
          getFavoritesChar={getFavoritesChar}
        />
      ));
    }
  };

  return (
    <main className="Characters">
      <Banner text="Characters" />
      <div className="Search-container">
        <Search value={search} onChange={searcher} />

        <Button
          onClick={() => setSearchActive("name")}
          className={` Button  ${getIsActive("name")}`}
        >
          All
        </Button>
        <Button
          onClick={() => setSearchActive("comic")}
          className={` Button  ${getIsActive("comic")}`}
        >
          Comics
        </Button>
      </div>
      <CustomScroll
        dataLength={results.length}
        next={getMore}
        hasMore={hasMore}
      >
        <div className="Characters-cards">{renderCharacters()}</div>
      </CustomScroll>
    </main>
  );
};

export default Characters;
