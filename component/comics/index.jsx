import Banner from "../banner";
import Card from "../card";
import CustomScroll from "../customScroll";
import Search from "../search";
import { useEffect, useState } from "react";
import { useDebounce } from "../../hook/useDebounce";
import axiosConect from "../../config/axios";
import Button from "../button";
import useFavorite from "../../config/axiosFavorite";
import { useSelector } from "react-redux";


const Comics = ({ results, getMore, hasMore }) => {
  const [search, setSearch] = useState("");
  const [searchActive, setSearchActive] = useState("title");
  const [filterList, setFilterList] = useState([]);
  const [favoriteListComic, setFavoriteListComic] = useState([]);
  const isLogged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [, refetch] = useFavorite(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const getFavoritesComic = async () => {
    if (!isLogged) return;
    const { data } = await refetch({
      params: {
        category: "COMIC",
      },
    });

    setFavoriteListComic(data);
  };

  useEffect(() => {
    getFavoritesComic();
  }, []);



  const searcher = (e) => {
    e.preventDefault();
    setSearch(e.target.value);
  };

  const getIsActive = (label) => {
    if (searchActive === label) return "isActive";
    return "";
  };




  useDebounce(
    () => {
      const searchCharacter = async () => {
        const value = search.trim();
        let params = {};
        if (value) {
          if (searchActive === "title"){
            params={titleStartsWith: value, limit: 100 }
          }
          if(searchActive === "format"){
            params={format: value}
          }
          if(searchActive === "issue"){
            params={issueNumber: value}
          }
          try {
            const { data } = await axiosConect.get(`/comics`, {
              params,
            });
            setFilterList(data.data.results);
          } catch (error) {
          }
        } else {
          setFilterList([]);
        }
      };
      searchCharacter();
    },
    800,
    [search]
  );

  const renderCharacters = () => {
    if (search) {
      return filterList?.map((result) => (
        <Card data={result} type="comics" key={result.id}  favoriteList={favoriteListComic} getFavoritesChar={getFavoritesComic} category="COMIC" />
      ));
    } else {
      return results?.map((result) => (
        <Card data={result} type="comics" key={result.id} getFavoritesChar={getFavoritesComic}  favoriteList={favoriteListComic} category="COMIC" />
      ));
    }
  };
  return (
    <main className="Comics">
      <Banner text="Comics" />
      <div className="Search-container">
 
      <Search value={search} onChange={searcher} />
      <Button onClick={() => {setSearchActive("title"); setSearch("")}} className={`Button ${getIsActive("title")} `}>
        Title
      </Button>
      <Button onClick={() => {setSearchActive("format"); setSearch("")}} className={`Button ${getIsActive("format")} `}>
        Format
      </Button>
      <Button onClick={() => {setSearchActive("issue"); setSearch("")}} className={`Button ${getIsActive("issue")} `}>
        Issue Number
      </Button>
      </div>
      <CustomScroll
        dataLength={results.length}
        next={getMore}
        hasMore={hasMore}
      >
        <div className="Comics-cards">{renderCharacters()}</div>
      </CustomScroll>
    </main>
  );
};

export default Comics;
