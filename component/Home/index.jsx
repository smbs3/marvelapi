import { useSelector } from "react-redux";
import Banner from "../banner";
import Card from "../card";

const Home = ({ results }) => {

  const renderSeries = () => {
    return results?.map((result) => <Card data={result} key={result.id} type="series" category="SERIE" />);
  };

  return (
    <main className="Home">
      <Banner text="Home" />
      <div className="Home-cards">{renderSeries()}</div>
    </main>
  );
};
export default Home;
