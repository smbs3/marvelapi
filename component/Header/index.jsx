import React from "react";
import Button from "../button";
import Image from "../image";
import Navbar from "../navbar";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { setIsLogged } from "../../redux/slice/userSlice";

const Header = () => {
  const login = useSelector((state) => state.user.isLogged);
  const dispatch = useDispatch();

  const handleLogOut = ()=>{
    dispatch(setIsLogged(''))
  }
  const renderLog = () => {
    if (!login) {
      return (
        <Link href="/login">
          <Button className="Header-user">SIGN IN</Button>
        </Link>
      );
    }
    return <Button className="Header-user" onClick={handleLogOut}>LOG OUT</Button>;
  };

  return (
    <div className="Header">
      <Image
        className="Header-logo"
        src="https://upload.wikimedia.org/wikipedia/commons/b/b9/Marvel_Logo.svg"
      ></Image>
      <Navbar></Navbar>
      {renderLog()}
    </div>
  );
};

export default Header;
