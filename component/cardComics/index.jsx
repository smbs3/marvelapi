import Image from "next/image";
import Link from "next/link";


const CardComics = ({ charactersComics }) => {
  const renderComics = () => {
    return charactersComics?.map((comics) => (
      <div className="CardComics">
        <Image
          src={`${comics.thumbnail.path}.${comics.thumbnail.extension}`}
          width={80}
          height={80}
        ></Image>
        <Link href={`/comics/${comics.id}`}>
          <p className="CardComics-text">{comics.title}</p>
        </Link>
      </div>
    ));
  };
  return <>{renderComics()}</>;
};

export default CardComics;
