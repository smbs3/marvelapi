import { navLinks } from "../../utils/data";
import Link from "next/link";

const Navbar = () => {
    return (
        <nav className="Navbar">
        {navLinks.map((link, index) => {
          return (
            <ul className="Navbar-list">
              <Link href={link.path}>
                <li key={index}>{link.name}</li>
              </Link>
            </ul>
          );
        })}
      </nav>
    );
}

export default Navbar;
