import InfiniteScroll from "react-infinite-scroll-component";

const CustomScroll = ({
  next,
  dataLength,
  hasMore,
  children,
}) => {
  return (
    <InfiniteScroll
      dataLength={dataLength}
      next={next}
      hasMore={hasMore}
    >
      {children}
    </InfiniteScroll>
  );
};

export default CustomScroll;
