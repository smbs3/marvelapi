import Input from "../input";

const Search = ({onChange, value }) => {
  return (
    <>
      <Input type="search" placeholder="Buscador" className="Search" onChange={onChange} value={value}/>
    </>
  );
};

export default Search;
