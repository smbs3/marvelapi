const Stories = ({ item }) => {
  return (
    <div className="List">
      <ul className="List-container">
        <li className="List-item">{item.name}</li>
      </ul>
    </div>
  );
};

export default Stories;
