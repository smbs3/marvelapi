import Image from "next/image";
import Link from "next/link";
import Button from "../button";
import { useSelector } from "react-redux";
import useFavoriteAxios from "../../config/axiosFavorite";
import { useCallback } from "react";
import { unstable_renderSubtreeIntoContainer } from "react-dom";

const Card = ({ data, type, category, favoriteList, getFavoritesChar }) => {
  const login = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [, refetch] = useFavoriteAxios(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
    );
    
    const {
      id,
      name,
      title,
      description,
      thumbnail: { extension, path },
    } = data;

    const deleteButtonHome= ()=>{
      if (type === "series")return 
      return renderButton()
    }


  const renderButton = () => {
    const isFavorite = favoriteList?.some(
      (favorite) => favorite.marvelId === data.id
    );
    if (isFavorite) {
      return (
        <Button
          className="Card-favorite"
          onClick={deleteItemFavorite(id, category)}
        >
          Eliminar
        </Button>
      );
    } else if (login) {
      return (
        <Button
          className="Card-favorite"
          onClick={addItemFavorite(id, category)}
        >
          Agregar a Favorito
        </Button>
      );
    }
  };



  const addItemFavorite = useCallback(
    (marvelId, category) => async () => {
      try {
        await refetch({
          method: "POST",
          data: {
            marvelId,
            category,
          },
        });
        getFavoritesChar();
      } catch (error) {
        return error;
      }
    },
    []
  );

  const deleteItemFavorite = useCallback(
    (marvelId, category) => async () => {
      try {
        const { data } = await refetch({
          params: {
            category,
          },
        });

        const index = data.findIndex((item) => marvelId === item.marvelId);
        const {id} = data[index]; 

        await refetch({
          url:`/favorites/${id}`,
          method:"DELETE",
        });
        alert("Eliminaste el Items");
        getFavoritesChar();
      } catch (error) {
        return error;
      }
    },
    []
  );

  return (
    <div className="Card">
      <div className="CardBody">
        <Image
          src={`${path}.${extension}`}
          width={350}
          height={250}
          alt={name}
          loading="lazy"
        />
        <div className="CardOverlay">
          <Link href={`/${type}/${id}`}>
            <h5 className="CardOverlay-title">{name || title}</h5>
          </Link>

          <p className="CardOverlay-text">{description}</p>
        </div>
        {deleteButtonHome()}
      </div>
    </div>
  );
};

export default Card;
