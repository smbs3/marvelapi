
const Banner = ({text}) => {
  return (
    <>
      <div className="Banner-imagen">
        <h1 className="Banner-text">Marvel API  {text}</h1>
        
      </div>
    </>
  );
};

export default Banner;
