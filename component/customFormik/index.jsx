import React, { useState } from 'react';
import { Form, Formik } from "formik";
import Input from "../inputFormik";
import Button from "../button";
import * as Yup from "yup";
import {setIsLogged} from "../../redux/slice/userSlice"
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import useFavoriteAxios from '../../config/axiosFavorite';

const CustomFormik = ({}) => {

  const [invalid, setInvalid] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  const [ , refetch] = useFavoriteAxios("/auth/login", {
    manual: true,
  });

  const SignupSchema = Yup.object().shape({
    username: Yup.string()
      .min(2, "Too Short!")
      .max(70, "Too Long!")
      .required("User is a required field"),
    password: Yup.string()
      .min(5, "Password must be at least 5 characters")
      .required("Password is a required field"),
  });

  const handleSubmit = async (values) => {
    const { username, password } = values;
    try {
      const { data } = await refetch({
        method: "POST",
        data: {
          username,
          password,
        },
      });

      dispatch(setIsLogged(data.access_token));
      router.push("/");
      setInvalid(false);
    } catch (error) {
      setInvalid(true);
    }
  };
  const initialValues = {
    username: "",
    password: "",
  };
  

  return (
    <Formik
      validationSchema={SignupSchema}
      initialValues={initialValues}
      onSubmit={handleSubmit}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
      }) => (
        <Form className="Login-form">
          <h2 className="Login-title">Login</h2>
          <Input
            id="username"
            name="username"
            type="text"
            className="Login-input"
            onChange={handleChange}
            onBlur={handleBlur}
            placeHolder="User Name"
          />
          <p className="Login-">
            {errors.username && touched.username && errors.username}
          </p>
          <Input
            id="password"
            name="password"
            type="password"
            className="Login-input"
            onChange={handleChange}
            onBlur={handleBlur}
            placeHolder="Password"
          />
          <p className="Login-">
            {errors.password && touched.password && errors.password}
          </p>
          <Button type="submit" className="Login-submit">
            Submit
          </Button>
        </Form>
      )}
    </Formik>
  );
};

export default CustomFormik;
