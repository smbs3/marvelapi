
const Input = ({type, className, placeholder, onChange, value}) => {
    return (
        <input type={type} value={value} className={className} placeholder={placeholder} onChange={onChange}></input>
    );
}

export default Input;
