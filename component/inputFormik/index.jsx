import {Field} from 'formik'

const Index = ({name, type, className, value, placeHolder, onBlur}) => {
    return (
        <Field 
        name={name}
        type={type}
        className={className}
        value={value}
        placeholder= {placeHolder}
        onBlur={onBlur} />
    );
}

export default Index;
