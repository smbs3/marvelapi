import Layout from "../component/Layout";
import HomeComponent from "../component/Home";
import axiosConect from "../config/axios";

const Home = ({ results }) => {

  return <HomeComponent results={results}/>;
};

Home.getLayout = (page) => <Layout>{page}</Layout>;

export default Home;

export async function getServerSideProps() {
  const { data } = await axiosConect.get("series");
  return { props: { results: data.data.results } };
}
