import React from "react";
import Layout from "../../component/Layout";
import axiosConect from "../../config/axios";
import List from "../../component/List";
import Link from "next/link";
import { AiFillCaretLeft } from "react-icons/ai";
import ReactHtmlParser from 'react-html-parser';

const Id = ({ results}) => {
  const [result] = results;
  const {
    title,
    description,
    thumbnail: { extension, path },
    stories: { items: storieItems },
    creators: { items: creatorItems },
  } = result;

  const img = `${path}.${extension}`;

  const renderOtros = (data) => {
    return data?.map((item) => <List item={item} />);
  };

  return (
    <div className="Post" >
      <div className="PostHeader">
        <h1>{title}</h1>
        <div className="PostHeader-return">
          <Link href="/comics">
            <AiFillCaretLeft />
          </Link>
        </div>
      </div>
      <div className="Post-description">
        {" "}
        <h3>Descriptions</h3>
        <p>{ ReactHtmlParser(description)}</p>
      </div>
      <div className="PostBody">
        <div
          className="PostBody-image "
          style={{ backgroundImage: `url(${img})` }}
        ></div>
        <div className="PostContainer">
          
        </div>
      </div>
      <div className="PostComplements">
            <div className="PostComplements-storie">
              <h3>Stories</h3>
              {renderOtros(storieItems)}
            </div>
            <div className="PostComplements-series">
              <h3>Creators</h3>
              {renderOtros(creatorItems)}
            </div>
          </div>
    </div>
  );
};
Id.getLayout = (page) => <Layout>{page}</Layout>;

export default Id;
export async function getServerSideProps({ query: { id } }) {
  const { data } = await axiosConect.get(`/comics/${id}`);
  const { data: characters } = await axiosConect.get(`/comics/${id}/characters`);
  return {
    props: {
      results: data.data.results,
      comicscharacters: characters.data.results,
    },
  };
}