import Layout from "../../component/Layout";
import ComicsContainer from "../../component/comics";
import axiosConect from "../../config/axios";
import { useState, useEffect } from "react";

const Comics = ({ results }) => {
  const [pages, setPages] = useState(1);
  const [list, setList] = useState(results);
  const [hasMore, setHasMore] = useState(true);
  const pagLimit = 20;
  const offset = 20;

  const getMore = () => {
    setPages((prev) => prev + 1);
  };

  

  useEffect(() => {
    const getComics = async () => {
      let params = { limit: pagLimit, offset: offset * pages };
      const { data } = await axiosConect("/comics", { params });
      const getComic = data.data.results;
      setList((prevList) => [...prevList, ...getComic]);
      setHasMore(list.length <= data.data.total);
    };
    getComics();
  }, [pages]);


  return (
    <ComicsContainer results={list} getMore={getMore} hasMore={hasMore} />
  );
};

Comics.getLayout = (page) => <Layout>{page}</Layout>;

export default Comics;

export async function getServerSideProps() {
  const { data } = await axiosConect.get("comics");
  return { props: { results: data.data.results } };
}
