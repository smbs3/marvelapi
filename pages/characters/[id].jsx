import React from "react";
import Layout from "../../component/Layout";
import axiosConect from "../../config/axios";
import List from "../../component/List";
import Link from "next/link";
import { AiFillCaretLeft } from "react-icons/ai";
import CardComics from "../../component/cardComics";

const Id = ({ results, charactersComics }) => {
  const [result] = results;
  const {
    name,
    description,
    thumbnail: { extension, path },
    stories: { items: storieItems },
    series: { items: seriesItems },
    comics: { items: comicsItems },
  } = result;

  const img = `${path}.${extension}`;

  const renderOtros = (data) => {
    return data?.map((item) => <List item={item} />);
  };

  return (
    <div className="Post">
      <div className="PostHeader">
        <h1>{name}</h1>
        <div className="PostHeader-return">
          <Link href="/characters">
            <AiFillCaretLeft />
          </Link>
        </div>
      </div>
      <div className="Post-description">
        {" "}
        <h3>Descriptions</h3>
        <p>{description}</p>
      </div>
      <div className="PostBody">
        <div
          className="PostBody-image "
          style={{ backgroundImage: `url(${img})` }}
        ></div>
        <div className="PostContainer">
          <div className="PostBody-series">
            <h3>Comics</h3>
            <div className="PostBody-comic">
              <CardComics charactersComics={charactersComics} />
            </div>
          </div>
        </div>
        
      </div>
      <div className="PostComplements">
            <div className="PostComplements-storie">
              <h3>Stories</h3>
              {renderOtros(storieItems)}
            </div>
            <div className="PostComplements-series">
              <h3>Series</h3>
              {renderOtros(seriesItems)}
            </div>
          </div>
    </div>
  );
};
Id.getLayout = (page) => <Layout>{page}</Layout>;

export default Id;
export async function getServerSideProps({ query: { id } }) {
  const { data } = await axiosConect.get(`/characters/${id}`);
  const { data: comics } = await axiosConect.get(`/characters/${id}/comics`);
  return {
    props: {
      results: data.data.results,
      charactersComics: comics.data.results,
    },
  };
}
