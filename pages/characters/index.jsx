import Layout from "../../component/Layout";
import CharactersContainer from "../../component/characters";
import axiosConect from "../../config/axios";
import { useEffect, useState } from "react";


const Characters = ({ results }) => {
  const [pages, setPages] = useState(0);
  const [list, setList] = useState(results);
  const [hasMore, setHasMore] = useState(true);
  const pagLimit = 20;
  const offset = 20;

  const getMore = () => {
    setPages((prev) => prev + 1);
  };

  
  
  useEffect(() => {
    if (!pages) return
    const getCharacters = async () => {
      let params = { limit: pagLimit, offset: offset * pages };
      const { data } = await axiosConect(`/characters`, { params });
      const getCharacter = data.data.results;
      setList((prevList) => [...prevList, ...getCharacter]);
      setHasMore(list.length <= data.data.total);
    };
    getCharacters();
  }, [pages]);

  return (
    <>
      <CharactersContainer
        results={list}
        getMore={getMore}
        hasMore={hasMore}
      />
    </>
  );
};

Characters.getLayout = (page) => <Layout>{page}</Layout>;

export default Characters;
export async function getServerSideProps() {
  const { data } = await axiosConect.get("/characters");
  return { props: { results: data.data.results } };
}
