import Card from "../component/card";
import Layout from "../component/Layout";
import Banner from "../component/banner";
import Button from "../component/button";
import { useSelector } from "react-redux";
import useFavorite from "../config/axiosFavorite";
import axios from "../config/axios";
import { useEffect, useState } from "react";

const Favorite = () => {
  const [list, setList] = useState([]);
  const [type, setType] = useState("");
  const [category, setCategory] = useState("");
  const isLogged = useSelector((state) => state.user.isLogged);
  const [marvelList, setMarvelList] = useState([]);
  const userToken = useSelector((state) => state.user.token);
  const [, refetch] = useFavorite(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const getData = (category, type) => async () => {
    if (!isLogged) return;
    const { data } = await refetch({
      params: {
        category,
      },
    });
    setCategory(category)
    setType(type)
    const marvelIds = data.map((item) => item.marvelId);
    const requests = marvelIds.map((item) => {
      return getDataMarvelApi(item, type);
    });
    const response = await Promise.allSettled(requests);
    const filteredResponse = response.reduce((prevValue, currentValue) => {
      if (currentValue.status === "fulfilled")
        return [...prevValue, currentValue.value[0]];
      return prevValue;
    }, []);
    setMarvelList(filteredResponse);
    setList(data);
    console.log(filteredResponse);
  };
  useEffect(() => {
    getData();
  }, []);

  const getDataMarvelApi = async (marvelId, type) => {
    if (!list) return;
    const { data } = await axios(`/${type}/${marvelId}`);
    const favoriteList = data?.data?.results;
    return favoriteList;
  };

  const renderFavorites = () => {


    if (!isLogged) {
      return (
        <div className="">
          <h3 className="">
            <span>You must be logged</span>
          </h3>
        </div>
      );
    }
    return marvelList.map((marvel) => (
      <Card data={marvel} type={type} category={category} key={marvel.id} favoriteList={list} />
    ));
  };
  const renderButton = () => {
    if (!isLogged) return;
    return (
      <div className="Favorite-category">
        <Button className="Button" onClick={getData("CHARACTER", "characters")}>
          Characters
        </Button>
        <Button className="Button" onClick={getData("COMIC", "comics")}>
          Comics
        </Button>
      </div>
    );
  };

  return (
    <>
      <div>
        <Banner text="Favorite" />
        {renderButton()}
      </div>

      <div className="Favorite-container">{renderFavorites()}</div>
    </>
  );
};
Favorite.getLayout = (page) => <Layout>{page}</Layout>;
export default Favorite;
