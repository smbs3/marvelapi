import { createSlice } from '@reduxjs/toolkit';
export const userSlice = createSlice({
  name: "user",
  initialState: {
    isLogged: false,
    token: "",
  },
  reducers: {
    setIsLogged: (state, { payload }) => {
      state.isLogged = !state.isLogged;
      state.token = payload;
    },
  },
});
export const { setIsLogged } = userSlice.actions;
export default userSlice.reducer;