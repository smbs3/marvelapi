export const navLinks = [
    { name: "Home", 
     path: "/" 
    },
    {
      name: "Characters",
      path: "/characters",
    },
    {
      name: "Comics",
      path: "/comics",
    },
    {
      name: "Favorite",
      path: "/favorite",
    },
  ];